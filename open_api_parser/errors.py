# -*- coding: utf-8 -*-
class BaseClassError(Exception):
    """Base class error to inherit."""


class MissingEnumValueError(BaseClassError):
    """Enum value is not found."""


class InvalidEnumTypeError(BaseClassError):
    """Enum value is not a string."""


class StrValueError(BaseClassError):
    """String value is not valid."""


class IntValueError(BaseClassError):
    """Integer value is not valid."""


class FloatValueError(BaseClassError):
    """Float value is not valid."""


class BoolValueError(BaseClassError):
    """Boolean value is not valid."""


class URLValueError(BaseClassError):
    """URL value is not valid."""


class EmailValueError(BaseClassError):
    """Email value is not valid."""


class PatternValueError(BaseClassError):
    """Pattern value is not valid."""


class RegularExpressionValueError(BaseClassError):
    """Pattern does not represent a valid regex."""


class MissingRequiredAttributeError(BaseClassError):
    """Required parameter value cannot be equal to None."""


class InvalidReferenceError(BaseClassError):
    """Reference link has too few path elements."""


class InvalidReferenceTypeError(BaseClassError):
    """Reference value type is not Reference or str."""


class MissingAttributeError(BaseClassError):
    """Element has no specified element."""


class NegativeIntValueError(BaseClassError):
    """Unsigned integer value is not valid."""
