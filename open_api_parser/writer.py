# -*- coding: utf-8 -*-
import json
from typing import Any, Iterable, NamedTuple

from open_api_parser.enumerations import OM, Type, Method


def to_string(value):
    if isinstance(value, str):
        return "\"<string>\""
    elif isinstance(value, int):
        return "<int>"
    elif isinstance(value, dict):
        return value
    elif isinstance(value, list):
        return value
    elif isinstance(value, bool):
        return "<bool>"
    else:
        return f"<{type(value)}>"


class APIAttribute(NamedTuple):
    name: str
    description: str
    data_type: Type = Type.NONE
    om: OM = OM.OPTIONAL
    example: str = None


class HTTPHeader(NamedTuple):
    name: str
    value: str

    def __str__(self):
        return f"-h \"{self.name}: {self.value}\""


class RequestAPI:
    def __init__(
            self,
            method: Method | None,
            webhook: str | None,
            headers: Iterable[HTTPHeader] | None = None,
            attributes: Iterable[APIAttribute] | None = None):
        if method is None:
            method: Method = Method.GET

        if webhook is None:
            webhook: str = "/"

        if headers is None:
            headers: list[HTTPHeader] = []

        self._method: Method = method
        self._webhook: str | None = webhook
        self._headers: list[HTTPHeader] = [*headers]
        self._attributes: list[APIAttribute] = [*attributes]
        self._payload: dict[str, Any] | None = dict()

    def __str__(self):
        _str_headers: str = "\n".join(map(str, self._headers))
        _str_payload: str = json.dumps(
            self._payload,
            ensure_ascii=False,
            indent=2,
            sort_keys=False,
            separators=(",", ": "))

        return (
            f"HTTP {self._method.value} https://127.0.0.1:8080{self._webhook} \\\n"
            f"{_str_headers}\n-d '{_str_payload}'")


"""
HTTP $METHOD http://$IP_ADDRESS:$PORT/$REQUEST \
-h '$HEADER: $VALUE' \
-d '{
  "$ATTRIBUTE": $VALUE
}'
"""
