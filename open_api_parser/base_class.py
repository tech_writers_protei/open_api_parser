# -*- coding: utf-8 -*-
from dataclasses import asdict, dataclass, field, fields, MISSING
from inspect import get_annotations
from types import UnionType
from typing import Any, get_args, get_origin, Mapping, Optional, Self, Type, Union

from loguru import logger
from yaml import safe_dump

from open_api_parser.errors import MissingRequiredAttributeError
from open_api_parser.simple_types import _BaseType, _DictType, _ListType, Bool, Dict, Int, List, Ref, Str

_attributes: dict[str, str] = {
    "type": "type_",
    "in": "in_",
    "license": "license_",
    "ref": "$ref",
    "format": "format_"
}

_conversion = {
    list: List,
    dict: Dict,
    str: Str,
    bool: Bool,
    int: Int
}


def upd(attr: str | Str):
    return _attributes.get(attr) if attr in _attributes else attr


def check_base_class(cls):
    try:
        return issubclass(cls, BaseClass)
    except TypeError:
        return False


def check_attribute(attribute: str | Str):
    return no_u(attribute) in _attributes


def check_base(cls):
    try:
        return issubclass(cls, _BaseType)
    except TypeError:
        return False


def check_dict(cls):
    try:
        return issubclass(get_origin(cls), _DictType)
    except TypeError:
        return False


def check_union_type(cls):
    try:
        return get_origin(cls) is Union
    except TypeError:
        return False


def check_list(cls):
    try:
        return issubclass(get_origin(cls), _ListType)
    except TypeError:
        return False


def no_u(value: str):
    return value.removesuffix("_")


def get_class(cls, values):
    _classes = get_args(cls)

    if get_origin(cls) is Union:
        if values is None:
            return NoneClass
        elif "$ref" in values:
            return Reference
        else:
            for item in get_args(cls):
                if item is not Reference:
                    return item

    elif issubclass(cls, NoneClass):
        return NoneClass

    elif issubclass(cls, BaseClass):
        return cls

    elif issubclass(cls, _BaseType):
        return _conversion.get(cls, cls)

    elif len(_classes) == 1:
        return _conversion.get(_classes[0], _classes[0])

    else:
        return NoneClass


# noinspection PyArgumentList,PyTypeChecker
@dataclass(init=True, repr=True, eq=True, kw_only=True)
class BaseClass:
    @classmethod
    def field_names(cls) -> list[str]:
        return [f.name for f in fields(cls)]

    @classmethod
    def fields(cls) -> dict[str, Type[Self | UnionType]]:
        return {f.name: get_annotations(cls, eval_str=True).get(f.name) for f in fields(cls)}

    @classmethod
    def required_names(cls) -> list[str]:
        return [no_u(f.name) for f in fields(cls) if f.default is MISSING]

    @classmethod
    def from_none(cls):
        return super().__init__({_f: None for _f in cls.field_names()})

    @classmethod
    def from_external(cls, values: Optional[Mapping[str, Any]]):
        if values is None:
            return cls.from_none()

        _values: dict[str, Any] = dict()

        for k, v in cls.fields().items():
            _: str = no_u(k)

            if _ not in values.keys():
                _values[k] = None
                continue

            else:
                _item = values.get(no_u(k))

                if cls is None:
                    continue

                elif check_attribute(k):
                    _values[upd(k)] = v.from_external(_item)
                    continue

                elif check_base_class(get_annotations(cls, eval_str=True).get(k)):
                    _values[k] = get_annotations(cls, eval_str=True).get(k).from_external(_item)
                    continue

                elif check_base(get_annotations(cls, eval_str=True).get(k)):
                    _values[k] = get_annotations(cls, eval_str=True).get(k).from_external(_item)
                    continue

                elif check_dict(get_annotations(cls, eval_str=True).get(k)):
                    _values[k] = Dict.from_external(_item)
                    continue

                elif check_union_type(get_annotations(cls, eval_str=True).get(k)):
                    _values[k] = get_class(get_annotations(cls, eval_str=True).get(k), _item).from_external(_item)
                    continue

                elif check_list(get_annotations(cls, eval_str=True).get(k)):
                    _values[k] = List.from_external(_item)
                    continue

        _element = cls(**_values)

        _missing: list[str] = [
            required_name for required_name in cls.required_names()
            if getattr(_element, required_name) is None]

        if _missing:
            _missing_str: str = "\n".join(_missing)
            logger.error(f"Missing attributes:\n{_missing_str}")
            raise MissingRequiredAttributeError

        else:
            del _missing

        return _element

    def __str__(self):
        return safe_dump(asdict(self), indent=2, encoding="utf-8", allow_unicode=True)


@dataclass
class NoneClass(BaseClass):
    @classmethod
    def from_external(cls, values):
        return None


@dataclass
class Reference(BaseClass):
    ref: Ref = field(init=True, repr=True, hash=True, compare=True, kw_only=True)
    summary: Optional[Str] = field(default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    description: Optional[Str] = field(default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)
