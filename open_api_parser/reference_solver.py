# -*- coding: utf-8 -*-
from collections import Counter
from typing import Any

from loguru import logger

from open_api_parser.base_class import BaseClass
from open_api_parser.errors import InvalidReferenceError, InvalidReferenceTypeError, MissingAttributeError
from open_api_parser.open_api_classes import Components, OpenAPI, Reference, StringType


class ReferenceSolver:
    def __init__(self, open_api: OpenAPI):
        self._open_api: OpenAPI = open_api
        self._components: Components | None = None
        self._reference: Reference | None = None
        self._parent: BaseClass | None = None
        self._attribute: StringType | None = None

    @property
    def reference(self):
        return self._reference

    @reference.setter
    def reference(self, value):
        self._reference = value

    @property
    def components(self):
        return self._components

    @components.setter
    def components(self, value):
        if self._components is None:
            self._components = value
        else:
            logger.info("Attribute 'components' has already been specified")

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        self._parent = value

    @property
    def attribute(self):
        return self._attribute

    @attribute.setter
    def attribute(self, value):
        self._attribute = value

    def __bool__(self):
        _bool_components: bool = self._components is not None
        _bool_reference: bool = self._reference is not None
        _bool_parent: bool = self._parent is not None
        _bool_attribute: bool = self._attribute is not None

        return _bool_components and _bool_reference and _bool_parent and _bool_attribute

    def nullify(self):
        self._reference = None
        self._parent = None
        self._attribute = None

    def __getitem__(self, item) -> BaseClass:
        def get_multiple_attr(*attrs: str, element: Any = None):
            if element is None:
                element = self

            if not hasattr(element, attrs[0]):
                logger.error(f"Invalid attribute {attrs[0]}")
                raise MissingAttributeError

            if len(attrs) == 1:
                return getattr(element, attrs[0])
            else:
                return get_multiple_attr(*attrs[1:], getattr(self, attrs[0]))

        def get_parts(value: str):
            if Counter(value).get("/") < 2:
                logger.error(f"Reference {value} is not proper")
                raise InvalidReferenceError

            _parts: list[str] = value.split("/")[2:]
            return get_multiple_attr(*_parts)

        if isinstance(item, Reference):
            return get_parts(item.ref)
        elif isinstance(item, str):
            return get_parts(item)
        else:
            raise InvalidReferenceTypeError

    def get(self, item) -> BaseClass:
        return self.__getitem__(item)

    def replace_ref(self):
        if not bool(self):
            raise
        else:
            setattr(self._parent, f"{self._attribute}", self.get(self._reference))


reference_solver: ReferenceSolver = ReferenceSolver()
