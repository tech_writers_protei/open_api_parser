# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from typing import Any, Iterator, Optional, TypeAlias

from open_api_parser.base_class import BaseClass, Reference
from open_api_parser.enumerations import ContentType, DataType, DiscriminatorName, In, IntegerFormat, Method, \
    NumberFormat, OM, SecurityType, StringFormat
from open_api_parser.simple_types import Bool, Dict, Email, Int, List, Pattern, Str, UInt, URL


class BooleanType(BaseClass):
    type: Str = Str("boolean")


class ArrayType(BaseClass):
    type: Str = Str("array")
    items: 'Schema' = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    maxItems: Optional[UInt] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    minItems: Optional[UInt] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    uniqueItems: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


class StringType(BaseClass):
    type: Str = Str("string")
    format: Optional[StringFormat] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    enum: Optional[List[Str]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    pattern: Optional[Pattern] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    default: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    maxLength: Optional[Int] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    minLength: Optional[Int] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


class NumberType(BaseClass):
    type: Str = Str("number")
    format: Optional[NumberFormat] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    multipleOf: Optional[UInt] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    maximum: Optional[Int] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    exclusiveMaximum: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    minimum: Optional[Int] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    exclusiveMinimum: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    default: Optional[Int] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


class IntegerType(BaseClass):
    type: Str = Str("integer")
    format: Optional[IntegerFormat] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    multipleOf: Optional[UInt] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    maximum: Optional[Int] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    exclusiveMaximum: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    minimum: Optional[Int] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    exclusiveMinimum: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    default: Optional[Int] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


class ObjectType(BaseClass):
    type: Str = Str("object")
    properties: Dict[Str, 'PropertyType'] = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    required: Optional[List[Str]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    additionalProperties: Optional[Dict[Str, 'PropertyType'] | Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    maxProperties: Optional[UInt] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    minProperties: Optional[UInt] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def property_names(self):
        if self.properties is None:
            return []
        else:
            return [*self.properties.keys()]

    def get_property(self, name: str):
        if self.properties is not None and name in self.property_names:
            return self.properties.get(name)
        else:
            return None


PropertyType: TypeAlias = BooleanType | StringType | IntegerType | NumberType | ArrayType | ObjectType


@dataclass
class ExternalDocs(BaseClass):
    url: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class Variable(BaseClass):
    enum: List[Str] = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    default: Str = field(
        init=True, repr=True, hash=False, compare=False, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def values(self):
        return " / ".join(f"`{item}`" for item in self.enum)


@dataclass
class Contact(BaseClass):
    name: Optional[Str] = field(
        default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)
    url: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    email: Optional[Email] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class Example(BaseClass):
    summary: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    value: Optional[Any] = field(
        default=None, init=True, repr=True, hash=False, compare=True, kw_only=True)
    externalValue: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class XML(BaseClass):
    name: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    namespace: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    prefix: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    attribute: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    wrapped: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class License(BaseClass):
    name: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    url: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class RequestBody(BaseClass):
    content: Dict[ContentType, Media] = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    required: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    def content_types(self):
        if self.content is None:
            return []
        else:
            return [*self.content.keys()]

    def get_content(self, content_type: ContentType):
        if self.content is not None and content_type in self.content_types():
            return self.content.get(content_type)
        else:
            return None


@dataclass
class OAuthFlow(BaseClass):
    authorizationUrl: URL = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    tokenUrl: URL = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    refreshUrl: Optional[URL] = field(
        default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)
    scopes: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)


@dataclass
class OAuthFlows(BaseClass):
    implicit: Optional[OAuthFlow] = field(
        default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)
    password: Optional[OAuthFlow] = field(
        default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)
    clientCredentials: Optional[OAuthFlow] = field(
        default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)
    authorizationCode: Optional[OAuthFlow] = field(
        default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)


@dataclass
class Tag(BaseClass):
    name: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    externalDocs: Optional[ExternalDocs] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class Server(BaseClass):
    url: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    variables: Optional[Dict[Str, Variable]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def variable_names(self):
        if self.variables is None:
            return []
        else:
            return [*self.variables.keys()]

    def get_variable(self, name: str):
        if self.variables is not None and name in self.variable_names:
            return self.variables.get(name)
        else:
            return None


@dataclass
class SecurityScheme(BaseClass):
    type: SecurityType = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)
    name: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    in_: In = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    scheme: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    bearerFormat: Optional[Str] = field(
        default=None, init=True, repr=True, hash=True, compare=True, kw_only=True)
    flows: OAuthFlows = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)


@dataclass
class SecurityRequirement(BaseClass):
    name: Str = field(
        init=True, repr=True, hash=False, compare=False, kw_only=True)
    attributes: Optional[List[Str]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    def iter_attributes(self):
        if self.attributes is None:
            return None
        else:
            return iter(self.attributes)


@dataclass
class Discriminator(BaseClass):
    propertyName: DiscriminatorName = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    mapping: Optional[Dict[Str, Str]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class Schema(BaseClass):
    type: Optional[DataType] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    discriminator: Optional[Discriminator] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    xml: Optional[XML] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    externalDocs: Optional[ExternalDocs] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    example: Optional[Any] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    deprecated: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    properties: Optional[Dict[Str, PropertyType]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def property_names(self):
        if self.properties is None:
            return []
        else:
            return [*self.properties.keys()]

    def get_property(self, item: str):
        if self.properties is not None and item in self.property_names:
            return self.properties.get(item)
        else:
            return None


@dataclass
class Header(BaseClass):
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    required: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    deprecated: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    allowEmptyValue: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    schema: Optional[Schema | Reference] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class Encoding(BaseClass):
    contentType: Optional[ContentType] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    headers: Optional[Dict[Str, Header | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    style: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    explode: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    allowReserved: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def header_names(self):
        if self.headers is None:
            return []
        else:
            return [*self.headers.keys()]

    def get_header(self, name: str):
        if self.headers is not None and name in self.header_names:
            return self.headers.get(name)
        else:
            return None


@dataclass
class Media(BaseClass):
    schema: Optional[Schema | Reference] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    example: Optional[Any] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    examples: Optional[Dict[Str, Example | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    encoding: Optional[Dict[Str, Encoding]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def example_names(self):
        if self.examples is None:
            return []
        else:
            return [*self.examples.keys()]

    def get_example(self, name: str):
        if self.examples is not None and name in self.example_names:
            return self.examples.get(name)
        else:
            return None

    @property
    def encoding_names(self):
        if self.encoding is None:
            return []
        else:
            return [*self.encoding.keys()]

    def get_encoding(self, name: str):
        if self.encoding is not None and name in self.encoding_names:
            return self.encoding.get(name)
        else:
            return None


@dataclass
class Link(BaseClass):
    operationRef: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    operationId: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    parameters: Optional[Dict[Str, 'Parameter']] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    requestBody: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    server: Optional[Server] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def parameter_names(self):
        if self.parameters is None:
            return []
        else:
            return [*self.parameters.keys()]

    def get_parameter(self, name: str):
        if self.parameters is not None and name in self.parameter_names:
            return self.parameters.get(name)
        else:
            return None


@dataclass
class Response(BaseClass):
    description: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    headers: Optional[Dict[Str, Header | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    content: Optional[Dict[ContentType, Media]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    links: Optional[Dict[Str, Link | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def header_names(self):
        if self.headers is None:
            return []
        else:
            return [*self.headers.keys()]

    def get_header(self, name: str):
        if self.headers is not None and name in self.header_names:
            return self.headers.get(name)
        else:
            return None

    @property
    def content_types(self):
        if self.content is None:
            return []
        else:
            return [*self.content.keys()]

    def get_content(self, content_type: ContentType):
        if self.content is not None and content_type in self.content_types:
            return self.content.get(content_type)
        else:
            return None

    @property
    def link_names(self):
        if self.links is None:
            return []
        else:
            return [*self.links.keys()]

    def get_link(self, name: str):
        if self.links is not None and name in self.link_names:
            return self.links.get(name)
        else:
            return None


@dataclass
class Responses(BaseClass):
    default: Optional[Response | Reference] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    responses: Optional[Dict[Str, Response | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)


@dataclass
class Info(BaseClass):
    title: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    termsOfService: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    contact: Optional[Contact] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    license: Optional[License] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    version: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)


@dataclass
class Parameter(BaseClass):
    name: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    in_: In = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    required: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    deprecated: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    allowEmptyValue: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    style: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    explode: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    allowReserved: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    schema: Optional[Schema | Reference] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    example: Optional[Any] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    examples: Optional[Dict[Str, Example | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    content: Optional[Dict[ContentType, Media]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def om(self):
        return OM.MANDATORY if self.required else OM.OPTIONAL

    def parameter(self):
        return f"| {self.name} | {self.description} | | {self.om} | | |"

    @property
    def content_types(self):
        if self.content is None:
            return []
        else:
            return [*self.content.keys()]

    def get_content(self, content_type: ContentType):
        if self.content is not None and content_type in self.content_types:
            return self.content.get(content_type)
        else:
            return None


@dataclass
class Callback(BaseClass):
    key: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    value: 'PathItem' | Reference = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)


@dataclass
class Operation(BaseClass):
    tags: Optional[Str | List[Str]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    summary: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    externalDocs: Optional[ExternalDocs] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    operationId: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    parameters: Optional[List[Parameter | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    requestBody: Optional[RequestBody | Reference] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    responses: Optional[Responses] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    callbacks: Optional[Dict[Str, Callback | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    security: Optional[List[SecurityRequirement]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    servers: Optional[List[Server]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    deprecated: Optional[Bool] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def callback_names(self):
        if self.callbacks is None:
            return []
        else:
            return [*self.callbacks.keys()]

    def get_callback(self, name: str):
        if self.callbacks is not None and name in self.callback_names:
            return self.callbacks.get(name)
        else:
            return None


@dataclass
class PathItem(BaseClass):
    ref: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    summary: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    description: Optional[Str] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    operations: Optional[Dict[Str, Operation]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    servers: Optional[List[Server]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    parameters: Optional[List[Parameter | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    tags: Optional[List[Str]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def operation_names(self):
        if self.operations is None:
            return []
        else:
            return [*self.operations.keys()]

    def get_operation(self, name: str):
        if self.operations is not None and name in self.operation_names:
            return self.operations.get(name)
        else:
            return None

    def iter_tags(self):
        if self.tags is None:
            return None
        else:
            return iter(self.tags)

    def iter_parameters(self):
        if self.parameters is None:
            return None
        else:
            return iter(self.parameters)

    def iter_servers(self):
        if self.servers is None:
            return None
        else:
            return iter(self.servers)


@dataclass
class Components(BaseClass):
    schemas: Optional[Dict[Str, Schema | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    responses: Optional[Responses] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    parameters: Optional[Dict[Str, Parameter | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    examples: Optional[Dict[Str, Example | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    requestBodies: Optional[Dict[Str, RequestBody | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    headers: Optional[Dict[Str, Header | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    securitySchemes: Optional[Dict[Str, SecurityScheme | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    links: Optional[Dict[Str, Link | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    callbacks: Optional[Dict[Str, Callback | Reference]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    @property
    def schema_names(self):
        if self.schemas is None:
            return []
        else:
            return [*self.schemas.keys()]

    @property
    def parameter_names(self):
        if self.parameters is None:
            return []
        else:
            return [*self.parameters.keys()]

    def get_parameter(self, name: str):
        if self.parameters is not None and name in self.parameter_names:
            return self.parameters.get(name)
        else:
            return None


@dataclass
class OpenAPI(BaseClass):
    openapi: Str = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    info: Info = field(
        init=True, repr=True, hash=True, compare=True, kw_only=True)
    servers: Optional[List[Server]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    paths: Optional[Dict[Str, Dict[Method, PathItem]]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    components: Optional[Components] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    security: Optional[List[SecurityRequirement]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    tags: Optional[List[Tag]] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)
    externalDocs: Optional[ExternalDocs] = field(
        default=None, init=True, repr=True, hash=False, compare=False, kw_only=True)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    @property
    def path_names(self):
        if self.paths is None:
            return []
        else:
            return [*self.paths.keys()]

    def iter_servers(self) -> Iterator[Server] | None:
        if self.servers is None:
            return None
        else:
            return iter(self.servers)

    @property
    def path_methods(self):
        if self.paths is None:
            return []
        else:
            return [*self.paths.keys()]

    def get_method(self, method: Method):
        if self.paths is not None and method in self.path_methods:
            return self.paths.get(method)
        else:
            return None

    def iter_security(self):
        if self.security is None:
            return None
        else:
            return iter(self.security)
