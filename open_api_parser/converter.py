# -*- coding: utf-8 -*-
from typing import Any

from typing_extensions import NamedTuple

from open_api_parser.open_api_classes import BooleanType, OpenAPI, PathItem, PropertyType, StringType
from open_api_parser.enumerations import Method, Position
from open_api_parser.simple_types import _ListType, Dict


class Converter:
    def __init__(self, open_api: OpenAPI):
        self._open_api: OpenAPI = open_api

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.paths
        else:
            return False

    @property
    def paths(self) -> list[str]:
        return [*self._open_api.paths.keys()]

    def get_path(self, item: str) -> Dict[Method, PathItem] | None:
        return self._open_api.paths.get(item, None)

    def methods(self, item: str) -> list[Method] | None:
        if item in self:
            return [*self.get_path(item).keys()]
        else:
            return None

    def get_method(self, item: str, method: str | Method) -> PathItem:
        if isinstance(method, str):
            method: Method = Method(method)
        return self.get_path(item).get(method)


class ParameterItem(NamedTuple):
    name: StringType
    location: Position
    type: PropertyType
    required: BooleanType
    example: Any


class RequestItem(NamedTuple):
    name: StringType
    path: StringType
    operationId: StringType
    method: Method
    summary: StringType
    description: StringType
    path_parameters: _ListType[ParameterItem] = []
    query_parameters: _ListType[ParameterItem] = []


class ResponseItem(NamedTuple):
    request_name: StringType
    query_parameters: _ListType[ParameterItem] = []


class APIElement(NamedTuple):
    request: Req
