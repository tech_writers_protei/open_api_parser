# -*- coding: utf-8 -*-
from re import match, compile, error
from typing import Iterable, Mapping, TypeAlias, TypeVar

from loguru import logger

from open_api_parser.errors import BoolValueError, EmailValueError, FloatValueError, IntValueError, \
    NegativeIntValueError, PatternValueError, \
    RegularExpressionValueError, StrValueError, \
    URLValueError

invalid: str = "()[]\"\'\\!?<>{}|;,$%^&*`~"
pattern: str = ""


class URL(str):
    def __new__(cls, value):
        if any(_ in value for _ in invalid):
            raise URLValueError
        return str.__new__(cls, value)

    @classmethod
    def from_external(cls, value):
        if value is None:
            return None
        elif isinstance(value, str):
            return cls(value)
        elif hasattr(value, "__str__"):
            return cls(str(value))
        else:
            return None


class Email(str):
    def __new__(cls, value):
        if not match(r"[\w._-]+@\w+\.\w+", value):
            raise EmailValueError
        return str.__new__(cls, value)

    @classmethod
    def from_external(cls, value):
        if value is None:
            return None
        elif isinstance(value, str):
            return cls(value)
        elif hasattr(value, "__str__"):
            return cls(str(value))
        else:
            return None


class Ref(str):
    def __new__(cls, value):
        if any(_ in value for _ in invalid):
            raise ValueError
        return str.__new__(cls, value)

    @property
    def parts(self) -> list[str]:
        return self.split("#/", 1)

    @property
    def file(self) -> str:
        return self.parts[0]

    @property
    def ref(self) -> str:
        return self.parts[-1]

    @property
    def elements(self) -> list[str]:
        return self.ref.split("/")


class Dict(dict):
    @classmethod
    def from_external(cls, value):
        if value is None:
            return None
        if issubclass(type(value), Mapping):
            return cls(value)
        else:
            return None


class List(list):
    @classmethod
    def from_external(cls, value):
        if value is None:
            return None
        elif issubclass(type(value), Iterable):
            return cls(value)
        else:
            return None


class Str(str):
    def __new__(cls, value):
        if not isinstance(value, str):
            raise StrValueError
        return str.__new__(cls, value)

    @classmethod
    def from_external(cls, value):
        if value is None:
            return None
        else:
            return cls(str(value))


class Int(int):
    def __new__(cls, value):
        if not isinstance(value, int):
            raise IntValueError
        return int.__new__(cls, value)

    @classmethod
    def from_external(cls, value):
        if value is None:
            return None
        elif (isinstance(value, str) or hasattr(value, "__str__")) and str(value).isnumeric():
            return cls(int(value))
        elif isinstance(value, int):
            return cls(value)
        else:
            return None


class UInt(Int):
    def __new__(cls, value):
        if not isinstance(value, int):
            raise IntValueError
        elif value < 0:
            raise NegativeIntValueError
        else:
            return super().__new__(cls, value)


class Float(float):
    def __new__(cls, value):
        if not isinstance(value, float):
            raise FloatValueError
        return float.__new__(cls, value)


class Bool(int):
    def __new__(cls, value):
        if not isinstance(value, bool):
            raise BoolValueError
        else:
            return bool(value)

    @classmethod
    def from_external(cls, value):
        if value is None:
            return None
        elif isinstance(value, bool):
            return cls(value)
        elif hasattr(value, "__bool__"):
            return cls(bool(value))
        else:
            return None


class Pattern(str):
    def __new__(cls, value):
        if not isinstance(value, str):
            raise PatternValueError

        try:
            compile(value)
        except error as e:
            logger.error(f"String {e.pattern} is not a regex")
            raise RegularExpressionValueError
        else:
            return str.__new__(cls, value)

    @classmethod
    def from_external(cls, value):
        if value is None:
            return None
        else:
            return cls(str(value))


_T: TypeAlias = TypeVar("_T")

_BaseType: TypeAlias = Str | Int | Bool | str | int | bool
_ListType: TypeAlias = List | list
_DictType: TypeAlias = Dict | dict
_CustomType: TypeAlias = Str | Int | Bool | List | Dict
_Type: TypeAlias = _BaseType | _ListType | _DictType
