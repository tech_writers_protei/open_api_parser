# -*- coding: utf-8 -*-
from dataclasses import asdict
from pathlib import Path
from typing import Any, Type

import benedict
from yaml import safe_dump

from open_api_parser.base_class import BaseClass


class YamlFile:
    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._content: dict[str, Any] = dict()
        self._base_class: Type[BaseClass] | None = None
        self._element: BaseClass | None = None

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._content.get(item, None)
        else:
            raise KeyError

    def __bool__(self):
        return len(self) > 0

    def read(self):
        self._content = benedict.load_yaml_file(self._path)

    def write(self):
        with open(self._path, "w") as f:
            f.write(safe_dump(self._content))

    @property
    def content(self):
        return self._content

    @property
    def path(self):
        return self._path

    def set_element(self):
        self._element = self._base_class.from_external(self._content)

    @property
    def base_class(self):
        return self._base_class

    @base_class.setter
    def base_class(self, value):
        self._base_class = value

    def __str__(self):
        return safe_dump(asdict(self._element))


class YamlFolder:
    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._yaml_files: list[YamlFile] = []

    def __iter__(self):
        return iter(self._yaml_files)

    def __len__(self):
        return len(self._yaml_files)

    def set_yaml_files(self):
        self._yaml_files = [YamlFile(file) for file in self._path.iterdir() if file.suffix == ".yaml"]

    def __getitem__(self, item):
        if isinstance(item, str):
            for yaml_file in self._yaml_files:
                if yaml_file.path.name == item:
                    return yaml_file
            else:
                return
        elif isinstance(item, int) and 0 <= item < len(self):
            return self._yaml_files[item]
        else:
            raise KeyError
