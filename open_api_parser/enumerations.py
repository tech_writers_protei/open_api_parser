# -*- coding: utf-8 -*-
from enum import Enum, unique

from open_api_parser.errors import InvalidEnumTypeError, MissingEnumValueError


class Enumeration(Enum):
    @classmethod
    def from_dict(cls, value):
        if value is None:
            return None
        elif not isinstance(value, str):
            raise InvalidEnumTypeError
        elif value not in cls._value2member_map_.keys():
            raise MissingEnumValueError
        else:
            return cls(value)

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self._name_})"


@unique
class In(Enumeration):
    PATH = "path"
    QUERY = "query"
    HEADER = "header"
    COOKIE = "cookie"


@unique
class Method(Enumeration):
    GET = "get"
    POST = "post"
    PUT = "put"
    DELETE = "delete"
    OPTIONS = "options"
    HEAD = "head"
    PATCH = "patch"
    TRACE = "trace"


@unique
class SecurityType(Enumeration):
    API_KEY = "apiKey"
    HTTP = "http"
    OAUTH2 = "oauth2"
    OPEN_ID_CONNECT = "openIdConnect"


@unique
class DataType(Enumeration):
    NULL = "null"
    INTEGER = "integer"
    NUMBER = "number"
    STRING = "string"
    BOOLEAN = "boolean"
    ARRAY = "array"
    OBJECT = "object"
    ONE_OF = "oneOf"
    ANY_OF = "anyOf"


@unique
class IntegerFormat(Enum):
    INT_32 = "int32"
    INT_64 = "int64"
    UINT_32 = "uint32"
    UINT_64 = "uint64"

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self._name_})"


@unique
class NumberFormat(Enum):
    FLOAT = "float"
    DOUBLE = "double"

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self._name_})"


@unique
class StringFormat(Enum):
    BYTE = "byte"
    BINARY = "binary"
    DATE = "date"
    DATETIME = "date-time"
    PASSWORD = "password"
    UUID = "uuid"
    EMAIL = "email"
    URI = "uri"
    HOSTNAME = "hostname"
    IPV4 = "ipv4"
    IPV6 = "ipv6"
    URL = "url"

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self._name_})"


@unique
class ContentType(Enum):
    JSON = "application/json"
    JSON_TEXT = "text/json"
    JSON_ANY = "application/*+json"
    JSON_PROBLEM = "application/problem+json"
    XML = "application/xml"
    FORM = "application/x-www-form-urlencoded"
    MULTIPART_FORM = "multipart/form-data"
    PLAIN_TEXT = "text/plain"
    HTML = "text/html"
    PDF = "application/pdf"
    PNG = "image/png"
    JPEG = "image/jpeg"
    GIF = "image/gif"
    SVG = "image/svg+xml"
    AVIF = "image/avif"
    BMP = "image/bmp"
    WEBP = "image/webp"
    IMAGE = "image/*"
    BINARY = "application/octet-stream"

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>({self._name_})"


@unique
class OAuthFlowType(Enumeration):
    IMPLICIT = "implicit"
    PASSWORD = "password"
    CLIENT_CREDENTIALS = "clientCredentials"
    AUTHORIZATION_CODE = "authorizationCode"


@unique
class DiscriminatorName(Enumeration):
    ALL_OF = "allOf"
    ONE_OF = "oneOf"
    ANY_OF = "anyOf"


class Type(Enumeration):
    STRING = "string"
    INT = "int"
    BOOL = "bool"
    LIST = "[]"
    OBJECT = "{}"
    NULL = "Null"
    ANY = "Any"
    REGEX = "regex"
    IP = "ip"
    FLOAT = "float"
    NONE = ""

    @classmethod
    def _missing_(cls, value):
        return cls.NONE


class OM(Enumeration):
    OPTIONAL = "O"
    MANDATORY = "M"
    CONDITIONAL = "C"

    @classmethod
    def _missing_(cls, value):
        return cls.OPTIONAL


class PR(Enumeration):
    PERMANENT = "P"
    RELOADABLE = "R"


class Position(Enumeration):
    PATH = "path"
    QUERY = "query"
