# -*- coding: utf-8 -*-
from dataclasses import is_dataclass
from typing import get_args, get_origin

from icecream import ic, install

from open_api_parser.base_class import BaseClass, Components, OpenAPI
from yaml_file import YamlFile


def main():
    install()
    ic.configureOutput(prefix="| ", includeContext=True)
    path: str = "globus-api.yaml"
    yaml_file: YamlFile = YamlFile(path)
    yaml_file.read()
    yaml_file.base_class = OpenAPI
    yaml_file.set_element()
    # ic(str(yaml_file._element))
    # print("====================")
    # ic(str(yaml_file._element))
    # ic(str(yaml_file.content.get("components")))
    ic(yaml_file._element.paths["/globus/v1/client/custom-list"])


def test_main():
    ic.configureOutput(prefix="| ", includeContext=True)
    path: str = "test-yaml.yaml"
    yaml_file: YamlFile = YamlFile(path)
    yaml_file.read()
    ic(type(yaml_file.content))
    # yaml_file.base_class = Components.from_external(yaml_file.content)
    yaml_file.base_class = Components
    # ic(str(yaml_file.content["components"].get("responses")))
    ic(str(yaml_file.base_class))
    yaml_file.set_element()
    ic(yaml_file.base_class.__mro__)
    ic(yaml_file._element)
    # ic(get_origin(getattr(yaml_file.base_class.responses, "responses")["emptyResponseType"]))
    ic(issubclass(yaml_file._base_class, BaseClass))
    ic(is_dataclass(yaml_file._element))
    # ic(f"{yaml_file._element.paths}")


if __name__ == '__main__':
    # a: str | int
    # print(get_args(a))
    main()
    # test_main()
